package ukim.finki.mpip2019.asynctask;

import android.os.AsyncTask;
import retrofit2.Call;
import ukim.finki.mpip2019.PlaylistInterface;
import ukim.finki.mpip2019.client.DzApiClient;
import ukim.finki.mpip2019.db.Repository;
import ukim.finki.mpip2019.models.DzPlaylist;
import ukim.finki.mpip2019.models.DzTrack;

import java.io.IOException;

public class DzPlaylistAsyncTask extends AsyncTask<Long, Integer, DzPlaylist> {
    Repository repository;

    public DzPlaylistAsyncTask(Repository repository){
        this.repository = repository;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        repository.deleteAll();
    }

    @Override
    protected DzPlaylist doInBackground(Long... longs) {
        final Call<DzPlaylist> playlist = DzApiClient.getService().getPlaylist(longs[0]);
        try {

            return playlist.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(DzPlaylist dzPlaylist) {
        for(DzTrack t: dzPlaylist.tracks.data) {
            repository.insert(t);
        }
//        dzPlaylist.tracks.data.forEach(d -> repository.insert(d));
    }
}
