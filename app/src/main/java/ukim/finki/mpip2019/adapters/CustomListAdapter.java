package ukim.finki.mpip2019.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ukim.finki.mpip2019.R;
import ukim.finki.mpip2019.holders.CustomListViewHolder;
import ukim.finki.mpip2019.models.DzTrack;

import java.util.ArrayList;
import java.util.List;

public class CustomListAdapter extends RecyclerView.Adapter {

    List<DzTrack> dataset;
    View.OnClickListener listener;

    public CustomListAdapter(View.OnClickListener listener) {
        this.dataset = new ArrayList<>();
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_layout, viewGroup, false);

        return new CustomListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((CustomListViewHolder)viewHolder).setText(dataset.get(i).title, listener);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void updateDataset(List<DzTrack> newDataset) {
        this.dataset = newDataset;
        notifyDataSetChanged();
    }

    public Long getClickedItemId(CustomListViewHolder holder) {
        int adapterPosition = holder.getAdapterPosition();
        return dataset.get(adapterPosition).id;
    }
}
