package ukim.finki.mpip2019.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ukim.finki.mpip2019.R;
import ukim.finki.mpip2019.util.FragmentCommunication;

public class DemoFragment extends Fragment {

    FragmentCommunication fragmentCommunication;

    public DemoFragment() {}

    @SuppressLint("ValidFragment")
    public DemoFragment(FragmentCommunication fragmentCommunication) {
        this.fragmentCommunication = fragmentCommunication;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.demo_fragment_layout,container,false);
    }

    @Override
    public void onResume() {
        super.onResume();

        fragmentCommunication.onItemFromFragmentSelected("Selected item");
    }

    public void listenFromActivity(String message) {

    }
}
