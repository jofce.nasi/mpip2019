package ukim.finki.mpip2019.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "dz_track")
public class DzTrack {

    @PrimaryKey
    public Long id;

    @ColumnInfo(name = "custom_title")
    public String title;

}
