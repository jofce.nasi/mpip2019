package ukim.finki.mpip2019.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import ukim.finki.mpip2019.asynctask.DzPlaylistAsyncTask;
import ukim.finki.mpip2019.models.DzTrack;

import java.util.List;

public class Repository {

    private static DzDatabase database = null;

    public Repository(Context context) {
        if(database == null) {
            database = Room
                    .databaseBuilder(context, DzDatabase.class, "db-app")
                    .build();
        }
//        fetchData();
    }

    public LiveData<List<DzTrack>> getAllTracks() {
        return database.getDzTrackDao().getAllAsync();
    }

    public void insert(DzTrack track) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                database.getDzTrackDao().insert(track);
                return null;
            }
        }.execute();
    }

    public void deleteAll() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                database.getDzTrackDao().deleteAll();
                return null;
            }
        }.execute();

    }

//    private void fetchData() {
//        DzPlaylistAsyncTask asyncTask = new DzPlaylistAsyncTask(this);
//        asyncTask.execute(1867419722L);
//    }

}
