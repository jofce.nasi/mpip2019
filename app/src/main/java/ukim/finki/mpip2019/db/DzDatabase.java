package ukim.finki.mpip2019.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import ukim.finki.mpip2019.models.DzTrack;

@Database(entities = {DzTrack.class}, version = 1)
public abstract class DzDatabase extends RoomDatabase {

    public abstract DzTrackDao getDzTrackDao();

}
