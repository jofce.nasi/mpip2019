package ukim.finki.mpip2019.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import ukim.finki.mpip2019.models.DzTrack;

import java.util.List;

@Dao
public interface DzTrackDao {

    @Query("SELECT * from dz_track")
    @Deprecated
    public List<DzTrack> getAll();

    @Query("SELECT * from dz_track")
    public LiveData<List<DzTrack>> getAllAsync();

    @Query("SELECT * from dz_track WHERE custom_title LIKE :title")
    public List<DzTrack> findByTitle(String title); // title == "abc"

    @Query("SELECT * from dz_track WHERE id = :id")
    public DzTrack findById(Long id);

    @Insert
    public void insert(DzTrack ...tracks);

    @Query("DELETE from dz_track WHERE id = :id")
    public void delete(Long id);

    @Query("DELETE from dz_track")
    public void deleteAll();

}
