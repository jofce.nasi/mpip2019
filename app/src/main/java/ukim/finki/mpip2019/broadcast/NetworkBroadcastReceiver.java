package ukim.finki.mpip2019.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.logging.Logger;


public class NetworkBroadcastReceiver extends BroadcastReceiver {

    private Logger logger = Logger.getLogger("NetworkBroadcastReceiver");

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        logger.info("RCV:" + intent.getAction());
    }
}
